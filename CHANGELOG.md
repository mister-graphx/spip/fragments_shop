# CHANGELOGS

1.1.29

* surcharge de bank/presta/xx/payer/acte
* ajout d'un texte editable/configurable a la popup_panier
* ~suppression du necessite de coordonnée~
* ~bloc header : surcharge du inclure/header de fragments_skel~

1.1.28

dans le menu mini-panier on utilise la commande en session

1.1.27

* surcharge de la page identification de moncompte, on inclu la page qui, pour avoir un markup partagé et uniforme, mais l'url de redirection des formulaire est celle de l'espace client URL_PAGE{moncompte}

1.1.26

* mini-panier bien filtrer la dernière commande en cours pour l'utilisateur.

1.1.25

* le menu mini-panier fait parti de fragments_shop

1.1.24

*  lang : chaine de langue par defaut mon_panier, Vos achats
*  content/panier : Ajout d'une config page panier textarea avec mise en forme, infos/ description du header * correction du markup
* 1OOO derniers produits dans le product_feed envoyé a facebook boutique


1.1.23

page panier : doc + url de retour par defaut sur le boutons continuer mes achats et limite su la boucle qui test si des objets sont dans le panier affichant le boutons commande

1.1.22

retour de commits perdus entre svn/git

- Ajout d'un descriptif pour le mode de paiement payal
- surcharge des chaines de langue de bank notament `explication_mode_paiement_paypal`

1.1.21

- corrige #2 le bouton commande n'est pas affiché si le panier est vide.
- ne plus afficher les liens vers le produit sur les pages commande,panier,mes_commandes_details
- compte client : inclusion du modèle paye_acte plutot qu'un lien vers la page payer.
- *En cours* ajout pipeline taches generales cron (#1 - Notification/ relance de commande en attente)
- *En cours* ajout d'une tache genie_fragments_shop_surveiller_commandes_dist effectuée toute les 24h (#1  - Notification/ relance de commande en attente)
- *En cours* ajout des travaux `genie_supprimer_commandes_attente` et `genie_relancer_commandes_attente`

1.1.20

- ne plus afficher les liens vers le produit sur les recaps de commande, commande, et page panier
- nettoyage de inclure plus utilisés

1.1.19

BUGFIX - retablir le modèle vacance

1.1.18

report des modifs non commitées au moment de la migration svn > git
aa86d2b0...7e34d5b6

1.1.16

- necessite suivant_precedent : utilisé dans le privé sur les pages produits
- ajout d'un flux pour les boutiques/catalogues facebook

Fri Jun 02 18:24:52 2017
:   *   Ajout de la page produit qui définie la structure z la plus haute pour pouvoir utiliser les compositions entre autre

1.1.14
Mon Nov 21 11:17:27 2016 :
    *   Ajout du plugin factures et intégration des inclures ./factures
    *   Mise a jour du plugin Coordonnéees et suppression Contact
    *   Mise a jour du plugin commandes
    *   ajout a la pipeline post_edition de la suppression du panier en cours

1.1.11
Thu Sep 29 09:15:34 2016 :
    *   head_meta pour les pages du parcours paiement
    *   correction sur les images og:images dans les metas

1.1.1
Sun Jun 05 09:21:19 2016
:   * On continue le nettoyage des fichiers inutiles
    * Validation du fonctionnement du nouveau parcours paiement
    * chaines de langues

Sat Jun 04 12:34:32 2016:
*   Mise en place du nouveau parcours paiement intégrant une étape livraison
*   Nettoyage du paquet.xml
    * Suppression du plugin client
    * suppression des pipelines inutilisées

Tue May 31 05:58:01 2016
:   *   Necessite le plugin identite_extra
        Ajoute des globals au fichier options
