# Fragments shop

Extension du squelette fragments_dist apportant les fonctionnalités e-commerce pour une boutique simple.

## Fonctionnalités

- gestion de catalogue produit
- gestion des stocks
- gestion des commandes, paiements, factures
- Accès publique Compte utilisateur
- SEO : données structurées sur les produits
- flux des produits pour les catalogues facebook business
- SEO : ajout des produits a la sitemap
