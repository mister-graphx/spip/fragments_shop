/**
@file Main
@desc main javascript file
*/

// # Dom Ready
jQuery(function($){
        // Initialize menu
        var mainmenu = $('.js-menumaker').responsiveMenu({
            format: 'multitoggle',
            breakpoint: 940
        });
        mainmenu.on();

        // Local Scroll on Anchor Links
        // $.localScroll();


        // Recharger le mini-panier, et le contenu du panier
        // quand on change le formulaire : quantité
        $(".formulaire_panier form").on('change',function(){
               $(this).submit().animateLoading();
        });

});
