/**


Mieux détecter les changement de viewport et surtout l'orientation
En utilisant matchMedia : https://developer.mozilla.org/en-US/docs/Web/API/Window/matchMedia
plutot qu'une détection de la largeur de l'écran.

https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Testing_media_queries


@function
@version
@name responsiveMenu
@param format multitoggle megaDropDown
@param sticky
@param breakpoint
@example:
(function($){
    $(document).ready(function(){
        $(".js-menumaker").responsiveMenu({
           format: "multitoggle",
           sticky:false,
           stickyClass: 'sticky',
           stickyTreshold: 50,
           breakpoint: 780,
           ulOpenClass:'open',
           onClickOutsideClose:true
        });
        var headDesktopHeight = $('.head-desktop').height();
        var menu = $('.js-menumaker').responsiveMenu({
            format: 'megaDropDown',
            sticky: false,
            stickyTreshold: headDesktopHeight
        });
        menu.on();
    });
})(jQuery);


@see https://github.com/dollarshaveclub/stickybits
@todo - enlever la dependance a jquery
@todo - aria sur le bouton mobile
@todo - priority+ pattern https://codepen.io/Dreamdealer/pen/waVzmK


*/
(function($) {

  $.fn.responsiveMenu = function(user_options) {
      'use strict';

      if ( typeof(user_options) == "undefined" || user_options === null ) {
          user_options = {};
      }

      var RM = {

        options: $.extend({
          format: "multitoggle",
          sticky: false,
          stickyClass: 'sticky',
          stickyTreshold: 50,
          breakpoint: 769,
          landscapeMaxWidth: 1025,
          ulOpenClass:'open',
          onClickOutsideClose:true
        }, user_options),
        //
        isOpen: false,
        mainContainer: $(this),
        menuContainer: $(this).find('.menu-container'),
        mainMenu: $(this).find('.menu-container > ul'),
        mobileButton: $(this).find(".mobile-button"),
        windowWidth: $(window).width(),
        // Util :
        closeMenu: function (){
          RM.mainMenu.removeClass('is-open');
          RM.mainMenu.removeClass('overlay');
          RM.mobileButton.removeClass('menu-opened');
          RM.menuContainer.removeClass('is-open');
          RM.mainMenu.find('.has-sub').removeClass("open");
        },

        collapseAll: function (){
          RM.mainMenu.find('li.has-sub .submenu-button.submenu-opened').removeClass('submenu-opened');
          RM.mainMenu.find('li.has-sub ul.' + RM.options.ulOpenClass).removeClass(RM.options.ulOpenClass);
        },

        // @todo une fonction utile pour placer des sous-menu dans le viewport
        // http://jsfiddle.net/G7qfq/582/
        inView: function (){
          $(".megadropdown li").on('mouseenter mouseleave', function (e) {
               if ($('ul', this).length) {
                   var elm = $('ul:first', this);
                   var off = elm.offset();
                   var l = off.left;
                   var w = elm.width();
                   var docH = $(".container").height();
                   var docW = $(".container").width();

                   var isEntirelyVisible = (l + w <= docW);

                   if (!isEntirelyVisible) {
                       $(this).addClass('edge');
                   } else {
                       $(this).removeClass('edge');
                   }
               }
          });
        },
        // Mode multiToggle
        // Une vue large navigation avec sous-menus arborescents
        //
        multiToggle: function (){
          // Ajouter les bouton d'état
          RM.mainMenu.find(".has-sub > a").append('<span class="submenu-button"></span>');
          // Positionner correctement les sous-menus
          // En mode large uniquement si les sous-menus s'affiche soit
          // a droite/soit a gauche suivant leur position dans le viewport
          if (RM.windowWidth > RM.options.breakpoint) {
            RM.mainMenu.find("> li.has-sub > a").on('mouseenter',function(e){
                var ParentElem = $(this).parent();
                var level1 = ParentElem.find('> ul');
                var subMenusWidth = level1.width();
                // 1er niveau
                if(typeof(level1) != undefined){
                    // console.log('HasSub     Offset left : ' + level1.offset().left);
                    // console.log('HasSub Witdh : ' + level1.width());
                    // console.log('Window Width : ' + RM.windowWidth );

                    // pour les sous menu les plus proches de la droite de l'écran
                    // on positionne le sous-menu a doite de l'item
                  if( (level1.offset().left + subMenusWidth ) > RM.windowWidth) {
                      level1.css('right', 0);
                  }
                  // Au survol

                  // submenus lists
                  var sub = level1.find('.has-sub > ul');
                  // console.log(sub[0]);
                  if(typeof(sub) != 'undefined' && sub.length > 0){
                      sub.each(function(i){
                          // console.log($(this).offset().left);
                        var subOffset = level1.offset().left + (subMenusWidth * 2) ;
                        if ( subOffset > RM.windowWidth ){
                          $(this).css('margin-left', '-100%');
                        }
                      });
                  }
                }
            });
          }
          // Evenement click sur une liste ayant des enfants
          RM.mainMenu.find("li.has-sub > a").on('click',function(e){
            // En vue mobile/collapse on déplie les menus
            if (RM.windowWidth < RM.options.breakpoint) {
                e.preventDefault();

                var ParentElem = $(this).parent();
                var icnState = $(this).find(".submenu-button");
                
                icnState.toggleClass('submenu-opened');
                ParentElem.find('> ul').toggleClass(RM.options.ulOpenClass);
            }
            // En vue large, le clic sur une item ferme les autres menus eventuellements ouverts
            if (RM.windowWidth > RM.options.breakpoint) {
                    //console.log('click');
              // var others = RM.mainMenu.find('li.has-sub').not(ParentElem);
              // others.each(function(){
              //   $(this).find('.submenu-button.submenu-opened').removeClass('submenu-opened');
              //   $(this).find('ul.' + RM.options.ulOpenClass).removeClass(RM.options.ulOpenClass);
              // });
            }
          });
        },
        // Mode megaDropDown
        // En vue large auto centre les mega-dropdown menu dans le viewport
        megaDropDown: function (){
            // en mode megaDropDown on parcours juste le premier niveau de listes
            // Ajoute le bouton d'état deplie/replie
            RM.mainMenu.find(".has-sub > a").append('<span class="submenu-button"></span>');
            // Evenement Clic sur les liens de premier niveau
            RM.mainMenu.find("> li.has-sub > a").on('click',function(e){
                window.scrollTo(0,0);
                RM.mainMenu.toggleClass('overlay');

                var ParentElem = $(this).parent().toggleClass(RM.options.ulOpenClass);
                var icnState = ParentElem.find(".submenu-button").toggleClass('submenu-opened');

              // En vue large
              if (RM.windowWidth > RM.options.breakpoint) {
                // le clic sur une item ferme les autres menus eventuellements ouverts
                var others = RM.mainMenu.find('li.has-sub').not(ParentElem);
                others.each(function(){
                  $(this).find('.submenu-button.submenu-opened').removeClass('submenu-opened');
                  $(this).find('ul.' + RM.options.ulOpenClass).removeClass(RM.options.ulOpenClass);
                });

              }
            //
            // icnState.toggleClass('submenu-opened');
            // ParentElem.find('ul').toggleClass(RM.options.ulOpenClass);
            e.preventDefault();
          });
        },
        megaDropDownReload: function (){
            if (RM.windowWidth > RM.options.breakpoint) {
                // verifier que le sous-menu est bien dans le viewport
                var megaDropDownMenu = RM.mainMenu.find("> .has-sub > ul");
                var parent = megaDropDownMenu.parent();

                var parentOffsetleft = parent[0].offsetLeft;
                var megaDropDownW = megaDropDownMenu.outerWidth();

                var docW = RM.windowWidth - 20;

                if(RM.windowWidth < 1200 ){
                  var setWidth = megaDropDownMenu.css({
                    maxWidth: docW,
                    //position:'absolute',
                    left: -( parentOffsetleft - 10 ),
                  });
                }
            }
        },
        dropDown: function (){
            // en mode dropDown on parcours juste le premier niveau de listes
            // Ajoute le bouton d'état deplie/replie
            // Sur le premier niveau
            RM.mainMenu.find(".has-sub > a").append('<span class="submenu-button"></span>');
            // Evenement Clic sur les liens de premier niveau
            RM.mainMenu.find("> li.has-sub > a").on('click',function(e){
                if (RM.windowWidth < RM.options.breakpoint) {
                    e.preventDefault();

                    // Se positionner en haut de page
                    window.scrollTo(0,0);
                    RM.mainMenu.toggleClass('overlay');

                    var ParentElem = $(this).parent().toggleClass(RM.options.ulOpenClass);
                    var icnState = ParentElem.find(".submenu-button").toggleClass('submenu-opened');
                }


              // En vue large
              if (RM.windowWidth > RM.options.breakpoint) {
                // // le clic sur une item ferme les autres menus eventuellements ouverts
                // var others = RM.mainMenu.find('li.has-sub').not(ParentElem);
                // others.each(function(){
                //   $(this).find('.submenu-button.submenu-opened').removeClass('submenu-opened');
                //   $(this).find('ul.' + RM.options.ulOpenClass).removeClass(RM.options.ulOpenClass);
                // });

              }
            //
            // icnState.toggleClass('submenu-opened');
            // ParentElem.find('ul').toggleClass(RM.options.ulOpenClass);
            // e.preventDefault();
          });
        },

        resizeFix: function () {
            // Update windowWidth
            RM.windowWidth = $(window).width();
            RM.collapseAll();
            RM.closeMenu();
            if(RM.options.format === 'megadropdown'){
              RM.megaDropDownReload();
            }
        },

        addAriaMarkup: function(){
          // Add ARIA role to menubar and menu items
          RM.mainmenu.attr('role', 'menubar').attr('aria-hidden', false).find('li').attr('role', 'menuitem');
          var first_level_links = RM.mainmenu.find('> li > a');
          //
          // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
          $(first_level_links).next('ul')
              .attr({ 'aria-hidden': 'true', 'role': 'menu' })
              .find('a')
              .attr('tabIndex',-1);

          // Adding aria-haspopup for appropriate items
          $(first_level_links).each(function(){
            if($(this).next('ul').length > 0)
              $(this).parent('li').attr('aria-haspopup', 'true');
          });

          $(first_level_links).on('hover',function(){
              $(this).closest('ul')
                .find('.' + RM.options.ulOpenClass)
                .attr('aria-hidden', 'true')
                .removeClass(RM.options.ulOpenClass)
                .find('a')
                .attr('tabIndex',-1);
              $(this).next('ul')
                .attr('aria-hidden', 'false')
                .addClass(RM.options.ulOpenClass)
                .find('a').attr('tabIndex',0);
          });

          $(top_level_links).on('focus',function(){
              $(this).closest('ul')
                .find('.' + RM.options.ulOpenClass)
                .attr('aria-hidden', 'true')
                .removeClass(RM.options.ulOpenClass)
                .find('a')
                .attr('tabIndex',-1);

              $(this).next('ul')
                .attr('aria-hidden', 'false')
                .addClass(RM.options.ulOpenClass)
                .find('a').attr('tabIndex',0);
          });

          $(document).on('click touchstart',function(){
              $('.'+settings.cssMenuOpen).attr('aria-hidden', 'true')
                    .removeClass(RM.options.ulOpenClass)
                    .find('a')
                    .attr('tabIndex',-1);
          });
        },

        initMenu: function (){
          //addAriaMarkup();
          RM.windowWidth = $(window).width();
          RM.menuContainer.find('li ul').parent().addClass('has-sub');
          RM.initiateClickListeners();
          RM.initiateResizeListener();

          if (RM.options.format === 'multitoggle'){
              RM.mainContainer.addClass('multitoggle');
              RM.multiToggle();
          }
          else if (RM.options.format === 'megadropdown'){
              RM.mainContainer.addClass('megadropdown');
              RM.megaDropDown();
              RM.megaDropDownReload();
          }
          else if (RM.options.format === 'dropdown'){
              RM.mainContainer.addClass('dropdown');
              RM.dropDown();
          }
          else {
              console.log('Type de menu inconnu :' + RM.options.format);
          }
          if (RM.options.sticky === true){
              $(window).scroll(function() {
                  var stickyClass = RM.options.stickyClass;
                  var treshold = RM.options.stickyTreshold;
                  if( $(this).scrollTop() > treshold) {
                    RM.mainContainer.addClass(stickyClass);
                  } else {
                    RM.mainContainer.removeClass(stickyClass);
                  }
              });
          }
        },

        initiateClickListeners: function (){
          RM.mobileButton.on('click', function(e){
              if(RM.isOpen == false) {
                window.scrollTo(0,0);
                $(this).addClass('menu-opened');
                RM.menuContainer.addClass('is-open');
                RM.mainMenu.addClass('is-open');
                RM.isOpen= true;
              }else{
                RM.closeMenu();
                RM.collapseAll();
                RM.isOpen = false;
              }
              e.preventDefault();
          });
          // Fermer le menu au clic en dehors
          // si en mode mobile
          if (RM.options.onClickOutsideClose === true
             && RM.windowWidth < RM.options.breakpoint){
              RM.menuContainer.on('click', function(e){
                 if (RM.menuContainer.hasClass('is-open')
                    && e.target.nodeName === 'NAV'){
                      RM.closeMenu();
                      RM.collapseAll();
                      RM.isOpen = false;
                      e.preventDefault();
                  }
              });
          }
        },

        initiateResizeListener: function (){
          $(window).on('load resize', function(){
            RM.resizeFix();
          });
        }

      };

      return {
        on: function(){
          RM.initMenu();
          RM.resizeFix();
          return this;
        },
        debug: function (){
          console.log(RM.options.format);
        }
      };

  };

})(jQuery);
