<?php


if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * affiche_prix
 *
 * @param $valeur
 * @param $decimales
 * @param $devise
*/
// function affiche_prix($valeur,$decimales=2,$devise="EUR"){

// 	$devise_dir = 'right';
// 	switch ($devise) {
// 		case 'EUR':
// 			$devise_display = "&nbsp;€";
// 			break;
// 		case 'USD':
// 			$devise_display = "$";
// 			$devise_dir = 'left';
// 			break;
// 		default:
// 			$devise_display = str_replace(' ','&nbsp;',$devise);
// 			break;
// 	}

// 	$devise = '<span itemprop="priceCurrency" content="'.attribut_html(trim($devise)).'">'.$devise_display.'</span>';
// 	$price = '<span itemprop="price" content="'.attribut_html($valeur).'">'.sprintf("%.{$decimales}f",$valeur).'</span>';
// 	if ($devise_dir!=='right'){
// 		return $devise.$price;
// 	}
// 	return $price.$devise;
// }


/**
 * filtre_joli_prix
 *
 * formatte un prix en ajoutant des classes css et span pour mettre en forme des badges
 * 
 * Utilisation
 * 	depuis la version 1.3 de PRIX échapement necessaire (on ne retourne que le prix brut sans la devise) :
 *  	#PRIX*|joli_prix
 *
 * @see http://php.net/manual/en/function.localeconv.php
*/
function filtre_joli_prix($prix, $format=false){
    
    //$prix = floatval($prix);
    
    // Notation française
    // https://php.net/manual/fr/function.number-format.php
    // Attenion a number_format : les valeurs peuvent êtres arrondies
    // la fonction n'est pas chainable plugieurs fois
    // @return {string}
    //$prix_fr = number_format($prix, 2, ',', ' ');
    //$prix = floatval($prix);
    // Pouvoir débrayer la devise de référence
//     if (! defined('PRIX_DEVISE')) {
//       define('PRIX_DEVISE','fr_FR.UTF-8');
//     }
    
//     // Pouvoir débrayer l'écriture de la devise par défaut
//     if (! defined('DEVISE_DEFAUT')) {
//       define('DEVISE_DEFAUT','&euro;');
//     }
    
//     setlocale(LC_MONETARY, PRIX_DEVISE); 
	
//     $localconv = localeconv();
    
//     //var_dump($localconv);
//     $prix = money_format('%!i',$prix);
// // var_dump($prix);
// 	($localconv['mon_decimal_point']) ? $decimal_separator = $localconv['mon_decimal_point'] : $decimal_separator = '.';
//     $money = explode($decimal_separator,$prix);
//     $num = $money[0];
	
// 	if($format == 1){
// 		(isset($money[1])) ? $num .= '<span class="currency">'.DEVISE_DEFAUT.'</span><span class="decimals">'.$money[1].'</span>' : $num;
// 		$prix = $num;
// 	}else{
// 		(isset($money[1])) ? $num .= '<span class="decimals">'.$decimal_separator.$money[1].'</span>' : $num;
// 		$prix = $num.'<span class="currency">'.DEVISE_DEFAUT.'</span>';
// 	}
    
    return $prix;
}

/**
 * surcharge du filtre montant_formater
 * pour afficher le symbol de la monaie en cour et ne pas afficher les centimes
 * 
 * @param float $montant
 * @param array $options
 * @return void
 */
function filtre_montant_formater($montant, $options = array()){
	$options = array(
		'currency_display' => 'symbol',
		'maximum_fraction_digits' => 0
	);

	return filtre_montant_formater_dist($montant, $options);
}

/**
 * fragments_shop_titre_etape()
 *
 * renvoie la chaine de langue de l etape en cours du parcours paiement
 * utilisé pour le breadcrumb principalement
 * 
 * @param $etape - etape du parcours paiement
 * @return string  _T() - la chaine de langue de l etape
*/
function fragments_shop_titre_etape($etape){
	return _T('fragments_shop:titre_etape_'.$etape);
}

/**
 * fragments_shop_liste_etapes()
 *
 * définie les étapes du parcours paiement
 * si les objet contenus sont livrables on ajoute l'étape livraison
 *
 * @param $id_panier
 *
 * 
*/
function fragments_shop_liste_etapes($id_panier){
	static $lesetapes = array();
	$etapes = array('panier','qui','commande','livraison','paiement');
	if (!$id_panier) return $etapes;
	if (isset($lesetapes["$id_panier"]))
		return $lesetapes["$id_panier"];

	if ($id_auteur = intval(sql_getfetsel('id_auteur','spip_paniers','id_panier='.intval($id_panier)))){
		$etapes = array_diff($etapes,array('qui'));
	}

	$items = sql_allfetsel("*","spip_paniers_liens","id_panier=".intval($id_panier));
	$livrable = false;
	foreach($items as $item){
		$table = table_objet_sql($item['objet']);
		$primary = id_table_objet($item['objet']);
		$objet = sql_fetsel("*",$table,"$primary=".intval($item['id_objet']));
		if (!isset($objet['immateriel']) OR !$objet['immateriel']){
			$livrable = true;
			break;
		}
	}
	if (!$livrable){
		$etapes = array_diff($etapes,array('livraison'));
	}

	return $lesetapes["$id_panier"] = $etapes;
}




?>