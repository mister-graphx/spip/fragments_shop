<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

define('_SIMU_BANK_ALLOWED',false);

$GLOBALS['url_arbo_parents']=array(
  'produit'=>array('id_rubrique','rubrique'),
  'article'=>array('id_rubrique','rubrique'),
  'rubrique'=>array('id_parent','rubrique'),
  'breve'=>array('id_rubrique','rubrique'),
  'site'=>array('id_rubrique','rubrique'),
  'mot'=>array('id_groupe','groupes_mot'));

$GLOBALS['url_arbo_types']=array(
  'produit'=>'',
  'mot'=>'',
    'groupes_mot'=>'', // pas de type pour les groupes_mot
   'rubrique'=>''
   );

// Titrer les documents joints à partir du nom du fichier
if (!defined('_TITRER_DOCUMENTS')) {
	define('_TITRER_DOCUMENTS', true);
}

// Champs supplémentaires pour l'identité du site
// Informations supplémentaire sur le site
// http://contrib.spip.net/Identite-Extra
$GLOBALS['identite_extra'] = array(
	// mentions légales
	"webmaster_nom",
	"responsable_publi",
	"proprio_nom",
	"proprio_prenom",
	"proprio_email",
	"hebergeur_nom",
	"hebergeur_adresse",
	"hebergeur_telephone",
	// e-commerce
	"raison_sociale",
	"code_ape",
	"tva_intra",
	"siret",
	"capital",
	"adresse","code_postal","ville","region","pays",
	"telephone"
);

?>
