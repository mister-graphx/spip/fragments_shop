<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Pipelines
 *
 * @see http://programmer.spip.net/Qu-est-ce-qu-un-pipeline
 * @see http://programmer.spip.net/-Liste-des-pipelines-
*/


/**
 * Mon Compte gestion des panels
 *
*/
function fragments_shop_moncompte_ajouter_panel($panels){

    $panels['commandes'] = array(
        'title'=>'Mes commandes',
        'items'=>array(
                array('title'=>"Voir mes commandes",'url'=>'commandes_toutes'),
        )
    );


    if(defined('_DIR_PLUGIN_ABONNEMENTS')){
        $panels['abonnements'] = array(
            'title'=>'Mes abonnements',
            'items'=>array(
                    array('title'=>'Tous mes abonnements','url'=>'abonnements_tous'),
            )
        );
    }

    return $panels;
}

/**
 * Après traitment bank
 *
 * @todo prendre en compte le paiement par chèque
 *
 * @see https://github.com/abelass/shop
*/
function fragments_shop_bank_traiter_reglement($flux){
    //// Vider le panier au retour du paiement
    //$id_panier=sql_getfetsel('id_panier','spip_transactions','id_transaction='.$flux['args']['id_transaction']);
    //$action = charger_fonction('supprimer_panier', 'action/');
    //$action($id_panier);
    //
    return $flux;
}

/**
 * post_edition
 *
 * - supprime le panier en cours au passage en commande
 *   comprned pas pourquoi la fonction de PANIER ne le fait pas
 * - passe un abonnement au statut actif QUAND la commande passe du statut attente a payee
 *
 * @see http://contrib.spip.net/Commandes-4527#forum478302
 * @see http://programmer.spip.net/et-les-autres
 *
*/
function fragments_shop_post_edition($flux){
    // Transformation d'un PANIER en COMMANDE
    // dans tous les cas on supprime le panier en cours a la transformation en commande
    // peut être faudrait il gérer le cas de annuler commande et supprimer le panier au passage a l'étape livraison ??
    if ($flux['args']['table']=='spip_commandes'
	    AND $flux['args']['action']=='instituer'
        AND ($statut_ancien = $flux['args']['statut_ancien']) == 'encours'
        AND ($id_commande = intval($flux['args']['id_objet'])) > 0
        AND $commande = sql_fetsel('id_commande, source', 'spip_commandes', 'id_commande='.intval($id_commande))
    ){

    spip_log($flux,'paniers');
		spip_log($commande,'paniers');

		if (preg_match(",^panier#(\d+)$,",$commande['source'],$m)){
			//$id_panier = intval($m[1]);
			//spip_log('Panier  : '.$id_panier, 'paniers');
			//spip_log('Panier en cours : '.paniers_id_panier_encours(), 'paniers');
			//
			//$supprimer_panier = charger_fonction('supprimer_panier_encours', 'action/');
			//$supprimer_panier($id_panier);
			//
			//// supprimer le panier de la session si c'est le meme
			//// ca permet potentiellement de retrouver une session vide (perf issue)
			////include_spip('inc/paniers');
			////if ($id_panier==paniers_id_panier_encours()){
			////	paniers_supprimer_panier_en_cours();
			////}
			//
			//// nettoyer une eventuelle double commande du meme panier
			////sql_updateq("spip_commandes",array('source'=>''),"source=".sql_quote($commande['source']));
			//
			//
			//spip_log('suppression panier '.$id_panier,'paniers');
		}
    }

    // Apres COMMANDE :
    // quand la commande passe du statut=attente a statut=paye
    if (
        $flux['args']['action'] == 'instituer'
        AND $flux['args']['table'] == 'spip_commandes'
        AND ($id_commande = intval($flux['args']['id_objet'])) > 0
        AND ($statut_nouveau = $flux['data']['statut']) == 'paye'
        AND ($statut_ancien = $flux['args']['statut_ancien']) == 'attente'
    ){
        // Informations concernant la commande
        $id_auteur= sql_getfetsel('id_auteur', 'spip_commandes', 'id_commande='.intval($id_commande));

        // retrouver les objets correspondants a la commande dans spip_commandes_details
        if (
            $objets = sql_allfetsel('objet,id_objet', 'spip_commandes_details', 'id_commande='.intval($id_commande))
            AND is_array($objets)
            AND count($objets)
        ){

            include_spip('action/editer_objet');

            foreach($objets as $v) {
                // Si un objet abonnements_offre
                // fait partit des details la commande
                if($v['objet']=='abonnements_offre'){

                    $objet = $v['objet'];
                    $id_objet = intval($v['id_objet']);

                   //l'auteur est il deja abonne a cette offre
                    $abo = sql_fetsel('*','spip_abonnements', array("id_auteur=$id_auteur",
                                                                    "id_abonnements_offre=$id_objet")
                                            );

                     // id_abonnements_offre
                    set_request('id_abonnements_offre', $id_objet);
                    // id_auteur
                    set_request('id_auteur', $id_auteur);

                    $editer_abonnement_dist = charger_fonction('traiter', 'formulaires/editer_abonnement');

                    if(sql_count($abo)>0 AND $abo['statut']='actif'){
                        $id_abonnement = $abo['id_abonnement'];
                        spip_log("l Auteur : $id_auteur est deja abonne offre : $id_objet | Abbonement $id_abonnement",'fragments_shop');
                        $retours = $editer_abonnement_dist($id_abonnement);
                    }else{
                        spip_log("Creation de abonnement : offre : $id_objet - Auteur : $id_auteur",'fragments_shop');
                        $retours = $editer_abonnement_dist('new');
                        // passage au statut actif
                        objet_modifier('abonnement', $retours['id_abonnement'], array('statut'=>'actif'));
                    }

                }
            }
        }
    }

    return $flux;
}


/**
 * Programmer la surveillance des commandes
 * @param $taches_generales
 * @return mixed
 */
function fragments_shop_taches_generales_cron($taches_generales){
	$taches_generales['fragments_shop_surveiller_commandes'] = 24*3600; // 24h
	return $taches_generales;
}

?>
