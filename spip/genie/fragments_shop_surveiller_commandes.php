<?php
/**
 * Surveiller les commandes
 *
 * @plugin     fragments_shop
 * @copyright  2018
 * @author     Arnaud B. (Mist. GraphX)
 * @licence    GNU/GPL
 * @package    SPIP\Fragments_shop\Genie
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


function genie_fragments_shop_surveiller_commandes_dist(){
	spip_log("Surveillance des commandes ",'fragments_shop');
    genie_relancer_commandes_attente();
    genie_supprimer_commandes_attente();

	return 1;
}


/**
 * Trouver toutes commandes attente : réglements cheques ou virement
 * (ce qui permet de donner une url directe vers la page de paiement sans refaire le processus)
 * et on relance en invitant a finaliser
 *
 * @param null|int $now
 */
function genie_relancer_commandes_attente($now = null){
	if (!$now) $now = time();

	$datemoins2w = date('Y-m-d H:i:s',strtotime('-2 week',$now));
    spip_log("relancer_commandes_attente $datemoins2w",'fragments_shop');
	$notifications = charger_fonction('notifications', 'inc');

	// trouver toutes les commandes dont le statut est en attente (cheque ou virement)
    // depuis plus de deux semaines
	$rows = sql_allfetsel('id_commande,statut,date,id_auteur','spip_commandes',"statut=".sql_quote('attente')." AND date<".sql_quote($datemoins2w));

	foreach($rows as $row){
        // @todo invalider les commandes en attente dont un produit ne serait plus en stock
        // traiter les cas des produit (spip_commandes_details.statut = attente)


        spip_log("Relancer commande en attente $row[id_commande]",'fragments_shop');
		// sinon on envoi une relance, et on note en relance
		// else {
		// 	// $notifications('inviterrecommencersouscription', $row['id_souscription']);
		// 	// spip_log("inviterrecommencersouscription id_souscription=".$row['id_souscription'],'souscriptions_surveillance');
		// 	// noter qu'on a fait le rappel
		// 	// sql_updateq("spip_souscriptions",array('statut'=>'relance'),'id_souscription='.intval($row['id_souscription']));
		// }
	}
}

/**
 * Trouver toutes commandes attente : réglements cheques ou virement
 * de plus de 15j et les passer en statut abandonnée/poubelle
 *
 * @param null|int $now
 */
function genie_supprimer_commandes_attente($now = null){
	if (!$now) $now = time();

    // Trouver toute les commandes dnt le statut est en attente (cheque ou virement)
    // depuis deux semaine + 1 jour
    $datemoins15d = date('Y-m-d H:i:s',strtotime('-2 week 1 day',$now));

    spip_log("supprimer_commandes_attente $datemoins15d",'fragments_shop');
	$notifications = charger_fonction('notifications', 'inc');

	// trouver toutes les commandes dont le statut est en attente (cheque ou virement)
    // depuis plus de 15j (2week + 1d)
	$rows = sql_allfetsel('id_commande,statut,date,id_auteur','spip_commandes',"statut=".sql_quote('attente')." AND date<".sql_quote($datemoins15d));


	foreach($rows as $row){
        spip_log("Supprimer commande en attente $row[id_commande]",'fragments_shop');
		// sinon on envoi une relance, et on note en relance
		// else {
		// 	// $notifications('inviterrecommencersouscription', $row['id_souscription']);
		// 	// spip_log("inviterrecommencersouscription id_souscription=".$row['id_souscription'],'souscriptions_surveillance');
		// 	// noter qu'on a fait le rappel
		// 	// sql_updateq("spip_souscriptions",array('statut'=>'relance'),'id_souscription='.intval($row['id_souscription']));
		// }
	}
}
