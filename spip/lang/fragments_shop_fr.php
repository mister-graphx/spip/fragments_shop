<?php
if (!defined("_ECRIRE_INC_VERSION")) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(

//A
	'action_commander_panier' => 'Commander',
	'action_payer_commande' => 'Finaliser ma commande',
	'action_choisir_livraison' => 'Choisir la livraison',
	'acces_facture'=>'Facture',
	'adresse_de'=>'Adresse de',
	'adresse_enregistrer'	=> 'Valider cette adresse',
	'aucun_produit'=>'Aucun produit',
	'aucune_commande_valide'=>'Il n\'y a (actuellement) aucune commande.',
	'ajouter_au_panier'=>'Ajouter au panier',
	'annuler'=>'<< Annuler',

	//B
	'boutique'=>'Boutique',
	'bouton_annuler_commande' => '<i class="icon-trash"></i> Annuler la commande',

	//C
	'commande'=>'Commande',
	'commande_invalide'=>'Référence de commande invalide',
	'commande_ref'=>'Commande @ref@',
	'commande_ref_statut'=>'Votre commande @ref@ est @statut@',
	'confirmation_commande'=>'Confirmation de votre commande',
	'confirmation_la_commande'=>'Confirmation de la commande',
	'continuer'=>'Continuer >>',
	'coordonnees'=>'Coordonnées',


	//D
	'deja_inscrit'=>'Déja inscrit',
	'detail_commande'=>'Détails',


	//E
	'explication_aucune_commande_en_cours'	=> 'Aucune commande en cours',


	//M
	'merci_de_votre_commande'=>'Nous vous remercions de votre commande @ref@',
	'mes_commandes'=>'Mes commandes',
	'modifier_adresse'=>'Modifier',
	'modifiez_vos_coordonnees'=>'Modifiez vos coordonnées',
	'mon_compte' => 'Mon compte',

	//N
	'nouvelle_adresse_de' => 'Nouvelle adresse de',

	//P
	'prix' => 'Prix',
	'panier'=>'Panier',
	'paiement'=>'Paiement',
	'paiement_fictif'=>'Paiement fictif',
	'passer_la_commande'=>'Passer la commande',
	'produits_recherche'=>'Produits correspondant &agrave; votre recherche',
	'produit_voir'=>'Voir',

	// Q
	'quantite'=>'Quantité',

	// R
	'reference' => 'Référence',
	'retour_achats' => 'Retourner à mes achats',
	'recapitulatif_commande' => 'Récapitulatif :',
	'recapitulatif_commande_adresse_livraison' => 'Expédiée à&nbsp;:',
	'recapitulatif_commande_adresse_facturation' => 'Facture à&nbsp;:',

	// S
	'total'=>'Total',
	'deja_client'=>'Si vous avez déjà un compte client, identifiez-vous',

	// U
	'une_commande_sur'=>'Une commande sur @nom@',

	// T

	'titre_etape_panier' => 'Mes achats',
	'titre_etape_qui' => 'Identification',
	'titre_etape_commande' => 'Commande',
	'titre_etape_livraison' => 'Livraison',
	'titre_etape_paiement' => 'Paiement',
    'titre_votre_panier' => 'Vos achats',
	'titre_votre_commande' => 'Ma commande #@id@',
	'titre_aucune_commande' => 'Aucune commande',
	'titre_livraison' => 'Livraison',
	'titre_paiement' => 'Paiement',
	'titre_nos_derniers_produits' => 'Nos derniers produits',

	// V
	'verifiez_vos_coordonnees'=>'Vérifiez vos coordonnées',
	'veuillez_vous_connecter'=>'Veuillez d\'abord vous connecter',
	'vider_le_panier'=>'Vider le panier',
	'vos_commandes_payees'=>'Vos commandes payées',
	'vos_coordonnees'=>'Si vous n\'avez pas de compte client, saisissez vos coordonnées',
	'votre_commande'=>'Votre commande',
	'votre_commande_du'=>'Votre commande du ',
	'votre_commande_sur'=>'Votre commande sur @nom@',
	'vous_inscrire'=>'Vous inscrire',

);
?>
